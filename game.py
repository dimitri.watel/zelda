import curses
import model


def game(stdscr):
    #curses.noecho()
    #curses.cbreak()
    #curses.curs_set(0)
    #stdscr.keypad(True)

    stdscr.clear()
    stdscr.timeout(100)

    width = curses.COLS - 1
    height = 23
    level = model.Level(stdscr, width, height, height // 2, width // 2)
    level.set_wall(height // 4, width // 4, 10, 1)
    level.set_wall(height // 4, width // 4, 1, 10)
    level.create_item(model.SHIELD, 1, 10,12)
    level.display()

    i = 0
    while True:
        i += 1
        c = stdscr.getch()
        if c == ord('a'):
            break
        elif c == curses.KEY_LEFT:
            level.move_link(-1, 0)
        elif c == curses.KEY_RIGHT:
            level.move_link(1,0)
        elif c == curses.KEY_UP:
            level.move_link(0,-1)
        elif c == curses.KEY_DOWN:
            level.move_link(0,1)

        level.display()
        stdscr.addstr(0, 0, str(i))
        stdscr.refresh()

    #stdscr.keypad(False)
    #curses.nocbreak()
    #curses.echo()
    #curses.endwin()

curses.wrapper(game)
