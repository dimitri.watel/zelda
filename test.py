import unittest
import model

class TestLevel(unittest.TestCase):

    def test_link_initial_position(self):
        level = model.Level(None, 10, 10, 8, 2)
        level.set_wall(5,5,1,3)
        self.assertEqual(level.link.l, 8)
        self.assertEqual(level.link.c, 2)

    def test_link_move_right(self):
        level = model.Level(None, 10, 10, 8, 2)
        level.set_wall(5,5,1,3)
        level.move_link(1, 0)
        self.assertEqual(level.link.l, 8)
        self.assertEqual(level.link.c, 3)
    
    def test_link_move_left(self):
        level = model.Level(None, 10, 10, 8, 2)
        level.set_wall(5,5,1,3)
        level.move_link(-1, 0)
        self.assertEqual(level.link.l, 8)
        self.assertEqual(level.link.c, 1)
    
    def test_link_move_up(self):
        level = model.Level(None, 10, 10, 8, 2)
        level.set_wall(5,5,1,3)
        level.move_link(0, -1)
        self.assertEqual(level.link.l, 7)
        self.assertEqual(level.link.c, 2)
    
    def test_link_move_down(self):
        level = model.Level(None, 10, 10, 8, 2)
        level.set_wall(5,5,1,3)
        level.move_link(0, 1)
        self.assertEqual(level.link.l, 9)
        self.assertEqual(level.link.c, 2)

    def test_link_cannot_move_left_at_leftmost_point(self):
        level = model.Level(None, 10, 10, 8, 0)
        level.move_link(-1, 0)
        self.assertEqual(level.link.c, 0)
    
    def test_link_cannot_move_right_at_rightmost_point(self):
        level = model.Level(None, 10, 10, 8, 9)
        level.move_link(1, 0)
        self.assertEqual(level.link.c, 9)

    def test_link_cannot_move_up_at_topmost_point(self):
        level = model.Level(None, 10, 10, 0, 8)
        level.move_link(0, -1)
        self.assertEqual(level.link.l, 0)
    
    def test_link_cannot_move_down_at_bottommost_point(self):
        level = model.Level(None, 10, 10, 9, 8)
        level.move_link(0, 1)
        self.assertEqual(level.link.l, 9)

    def test_link_starts_with_3hp(self):
        level = model.Level(None, 10, 10, 9, 8)
        self.assertEqual(level.link.hp, 3)

    def test_link_dies_at_0hp(self):
        level = model.Level(None, 10, 10, 9, 8)
        
        for i in range(3):
            level.hurt_link()

        self.assertEqual(level.link.hp, 0)
        self.assertTrue(level.link_is_dead())

    def test_link_cannot_go_through_walls(self):
        # link commence en 9,8
        level = model.Level(None, 10, 10, 9, 8)
        level.set_wall(9, 9, 1, 1)
        level.move_link(1,0)
        self.assertEqual(level.link.l, 9)
        self.assertEqual(level.link.c, 8)
        
    def test_create_shield(self):
        level = model.Level(None, 10, 10, 9, 8)
        level.create_item(model.SHIELD,3,1,2)
        self.assertEqual(len(level.items), 1)
        self.assertEqual(level.items[0].type, model.SHIELD)
        self.assertEqual(level.items[0].pos, (1,2))

    def test_get_shield(self):
        level = model.Level(None, 10, 10, 9, 8)
        level.create_item(model.SHIELD, 3,1,2)
        level.move_link(-6,-8)
        level.link_set_item(level.items[0])
        self.assertEqual(len(level.link.items), 1)
        self.assertEqual(level.link.items[0].type, model.SHIELD)


    #def test_shield_protect(self):

    
if __name__ == '__main__':
    unittest.main()
