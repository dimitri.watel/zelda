import curses

EMPTY = 0
WALL = 1

symbols = [' ', 'W', 'S']
linksymb = 'L'
SHIELD = 2

class Level:
    def __init__(self, stdscr, width, height, ml, mc):
        self.width = width
        self.height = height
        self.cells = [[Cell(EMPTY, l, c) for c in range(width)] for l in range(height)]
        self.link = Link(ml, mc)
        self.stdscr = stdscr
        self.items = []

    def set_wall(self, l, c, h, w):
        for cp in range(w):
            for lp in range(h):
                self.cells[l + lp][c + cp].type = WALL

    def move_link(self, dc, dl):

        if (0 <= self.link.l + dl < self.height and 0 <= self.link.c + dc < self.width ) and self.cells[self.link.l + dl][self.link.c + dc].type == EMPTY:
            self.link.move(dc, dl)

    def hurt_link(self):
        self.link.hp -= 1

    def link_is_dead(self):
        return self.link.hp == 0

    def display(self):
        for line in self.cells:
            for cell in line:
                cell.display(self.stdscr)
        self.link.display(self.stdscr)
        for item in self.items:
            item.display(self.stdscr)
        self.stdscr.refresh()
        

    def create_item(self, type, lvl, x, y):
        item = Item(type, lvl, x ,y)
        self.items.append(item)
        
    def link_set_item(self, item):
        self.link.items.append(item)

    


class Cell:
    def __init__(self, t, l, c):
        self.__t = t
        self.l = l
        self.c = c

    @property
    def type(self):
        return self.__t

    @type.setter
    def type(self, t):
        self.__t = t

    def display(self, stdscr, refresh=False):
        stdscr.addstr(self.l, self.c, symbols[self.type])
        if refresh:
            stdscr.refresh()

class Link:
    def __init__(self, l, c):
        self.l = l
        self.c = c
        self.hp = 3
        self.items = []

    def move(self, dc, dl):
        self.l += dl
        self.c += dc

    def display(self, stdscr, refresh=False):
        stdscr.addstr(self.l, self.c, linksymb)

        if refresh:
            stdscr.refresh()
    

class Item:
    def __init__(self, type, lvl, x, y):
        self.type = type
        self.lvl = lvl
        self.x = x
        self.y = y

    @property
    def pos(self):
        return self.x, self.y
    
    def display(self, stdscr, refresh=False):
        stdscr.addstr(self.y, self.x, symbols[self.type])
        if refresh:
            stdscr.refresh()